# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
from sensors import sensors
from decisions import decisions
from moves import moves

from main_control import MainControl
main_control = MainControl()

GPIO.setmode(GPIO.BOARD)

motor_pin_list = [16, 12, 22, 15, 18, 13]
GPIO.setup(motor_pin_list, GPIO.OUT)

sensorsThread = False
decisionsThread = False
movementsThread = False

def main():
	print('Journey started!')
	time.sleep(11)
	main_control.tickInterval = 0.069
	
	sensorsThread = runThread("sensorsThread")
	sensorsThread.start()
	
	decisionsThread = runThread("decisionsThread")
	decisionsThread.start()
	
	movementsThread = runThread("movementsThread")
	movementsThread.start()
	
	while main_control.runSensors:
		time.sleep(1)

	close_threads([movementsThread, decisionsThread, sensorsThread])
	GPIO.cleanup()
	print('Journey ended!')
	

def close_threads(threads):
	print('LOL')
	for thread in threads:
		if thread.name == 'sensorsThread':
			main_control.runSensors = False
		if thread.name == 'decisionsThread':
			main_control.runDecisions = False
		if thread.name == 'movementsThread':
			main_control.runMoves = False
		thread.join()
	
class runThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = name
	def run(self):
		if self.name == "sensorsThread":
			sensors.start_sensors()
			sensors.run_sensors()
		if self.name == "decisionsThread":
			decisions.start_decisions()
			decisions.run_decisions()
		if self.name == "movementsThread":
			moves.start_moves()
			moves.run_moves()


def not_main():
	print('not main')
	
if __name__=='__main__':
	main()
else:
	not_main()
