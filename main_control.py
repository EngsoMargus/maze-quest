# python3

# This is the main cross-file cross-thread variable store

class MainControl():
	
	_tickInterval = 0.1
	
	_runSensors = False
	_runDecisions = False
	_runMoves = False
	
	
	@property
	def tickInterval(self):
		return type(self)._tickInterval
		
	@tickInterval.setter
	def tickInterval(self, val):
		type(self)._tickInterval = val
		
		
	@property
	def runSensors(self):
		return type(self)._runSensors
		
	@runSensors.setter
	def runSensors(self, val):
		type(self)._runSensors = val
		
	
	@property
	def runDecisions(self):
		return type(self)._runDecisions
		
	@runDecisions.setter
	def runDecisions(self, val):
		type(self)._runDecisions = val
		
		
	@property
	def runMoves(self):
		return type(self)._runMoves
		
	@runMoves.setter
	def runMoves(self, val):
		type(self)._runMoves = val
