# python3

class SensorReading():
	
	_SensorReading = False
	
	@property
	def SensorReading(self):
		return type(self)._SensorReading
		
	@SensorReading.setter
	def SensorReading(self, val):
		type(self)._SensorReading = val
