# python3

import sys
import RPi.GPIO as GPIO
import time
import threading
from sensors.sensory_data import SensoryData
from sensors.sensor_reading import SensorReading
sensor_reading = SensorReading()
sys.path.append('../')
from main_control import MainControl
main_control = MainControl()


# Left sensor variables
LEFT_TRIG = 36
LEFT_ECHO = 35

# Middle sensor variables
MIDDLE_TRIG = 38
MIDDLE_ECHO = 40

# Right sensor variables
RIGHT_TRIG = 37
RIGHT_ECHO = 33

def start_sensors():
	main_control.runSensors = True
	GPIO.setup(LEFT_TRIG, GPIO.OUT)
	GPIO.setup(LEFT_ECHO, GPIO.IN)
	GPIO.setup(MIDDLE_TRIG, GPIO.OUT)
	GPIO.setup(MIDDLE_ECHO, GPIO.IN)
	GPIO.setup(RIGHT_TRIG, GPIO.OUT)
	GPIO.setup(RIGHT_ECHO, GPIO.IN)
	print('Sensors started!')
	
def run_sensor(TRIG, ECHO):
	start = 0
	end = 0
	counter = 0
	GPIO.output(TRIG, True)
	time.sleep(0.00001)
	GPIO.output(TRIG, False)
	while (GPIO.input(ECHO) == False and counter < 10000):
		start = time.time()
		counter += 1
	while GPIO.input(ECHO) == True:
		end = time.time()
	if end != 0:
		return ((end - start) * 34300) / 2
	else:
		return 1000000

def run_sensors():
	while main_control.runSensors:
		time.sleep(main_control.tickInterval)
		left_distance = run_sensor(LEFT_TRIG, LEFT_ECHO)
		middle_distance = run_sensor(MIDDLE_TRIG, MIDDLE_ECHO)
		right_distance = run_sensor(RIGHT_TRIG, RIGHT_ECHO)
		store_data(SensoryData(left_distance, middle_distance, right_distance))
			
def store_data(sensory_data):
	print(sensory_data.left, sensory_data.middle, sensory_data.right)
	sensor_reading.SensorReading = sensory_data

	
