# python3

class DecisionMade():
	
	_DecisionMade = False
	
	@property
	def DecisionMade(self):
		return type(self)._DecisionMade
		
	@DecisionMade.setter
	def DecisionMade(self, val):
		type(self)._DecisionMade = val
