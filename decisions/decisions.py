# python3

import sys
import time
import threading
sys.path.append('../')
from sensors.sensory_data import SensoryData
from sensors.sensor_reading import SensorReading
sensor_reading = SensorReading()
from decisions.decision_made import DecisionMade
decision_made = DecisionMade()
from main_control import MainControl
main_control = MainControl()
from decisions import decisions

last_decision = False

WALL_DISTANCE = 4.5
NO_WALL_DISTANCE = 20
STUCK_DISTANCE = 3
STOP_DISTANCE = 30
TURN_DISTANCE = 20

def start_decisions():
	main_control.runDecisions = True
	last_decision = False
	
def run_decisions():
	while main_control.runDecisions:
		time.sleep(main_control.tickInterval)
		sr = sensor_reading.SensorReading
		if sr:
			if main_control.runSensors == False:
				store_decision(False)
			elif sr.middle < STOP_DISTANCE and sr.left > NO_WALL_DISTANCE and sr.right > NO_WALL_DISTANCE:
				store_decision(False)
				main_control.runMoves = False
				main_control.runDecisions = False
				main_control.runSensors = False
			elif sr.left < STUCK_DISTANCE and sr.right < STUCK_DISTANCE:
				main_control.runMoves = False
				main_control.runDecisions = False
				main_control.runSensors = False
			elif sr.left > NO_WALL_DISTANCE and sr.middle < TURN_DISTANCE:
				store_decision('turn_left')
				time.sleep(0.92)
			elif sr.right > NO_WALL_DISTANCE and sr.middle < TURN_DISTANCE:
				store_decision('turn_right')
				time.sleep(0.92)
			elif sr.left < WALL_DISTANCE:
				store_decision('keep_right')
			elif sr.right < WALL_DISTANCE:
				store_decision('keep_left')
			else:
				store_decision('forward_march')
				
def store_decision(decision):
	decision_made.DecisionMade = decision
	print(decision_made.DecisionMade)
