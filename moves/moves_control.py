# python3

# This is the movements cross-thread variable store

class MoveControl():
	
	_keepRunning = False
		
	@property
	def keepRunning(self):
		return type(self)._keepRunning
		
	@keepRunning.setter
	def keepRunning(self, val):
		type(self)._keepRunning = val
