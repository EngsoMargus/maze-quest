# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
sys.path.append('../')
from main_control import MainControl
main_control = MainControl()
from moves.moves_control import MoveControl
moves_control = MoveControl()

def run_start(leftSpeed, rightSpeed):
	print('Starter started! ', leftSpeed, rightSpeed)
	pins = [12, 15, 16, 22]
	p1 = GPIO.PWM(12, 100)
	p2 = GPIO.PWM(16, 100)
	p1.start(leftSpeed)
	p2.start(rightSpeed)
	for pin in pins:
		if pin:
			GPIO.output(pin,GPIO.HIGH)
	start_time = time.time()
	while (moves_control.keepRunning):
		time.sleep(main_control.tickInterval)
	for pin in pins:
		if pin:
			GPIO.output(pin,GPIO.LOW)
	p1.stop()
	p2.stop()
	print('Starter stopped!')
