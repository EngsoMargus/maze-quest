# python3

# Create 2 threaded movement
# Keep speed control in the main moves thread
# Keep starter thread short and just for keep running purposes

# https://sourceforge.net/p/raspberry-gpio-python/wiki/PWM/
# https://www.raspberrypi.org/documentation/usage/gpio/

import sys
import time
import threading
import RPi.GPIO as GPIO
sys.path.append('../')
from decisions.decision_made import DecisionMade
decision_made = DecisionMade()
from main_control import MainControl
main_control = MainControl()
from decisions.decision import Decision
from moves.moves_control import MoveControl
moves_control = MoveControl()
from moves import starter

old_decision = False
starterThread = False

leftSpeed = 80
rightSpeed = 80

def start_moves():
	main_control.runMoves = True
	print('Moves started!')
	
def run_moves():
	global old_decision
	while main_control.runMoves:
		time.sleep(main_control.tickInterval)
		if (decision_made.DecisionMade):
			dm = decision_made.DecisionMade
			if dm != old_decision:
				# TODO: Check the sequence order!
				change_move(dm)
			else:
				pass
	moves_control.keepRunning = False
	if starterThread:
		starterThread.join()
				
def change_move(dm):
	global old_decision
	global leftSpeed
	global rightSpeed
	global starterThread
	if old_decision == False:
		moves_control.keepRunning = False
	else:
		moves_control.keepRunning = False
		if (dm == 'keep_right'):
			leftSpeed = 90
			rightSpeed = 72
		elif (dm == 'keep_left'):
			leftSpeed = 72
			rightSpeed = 90
		elif (dm == 'turn_right'):
			leftSpeed = 100
			rightSpeed = 0
		elif (dm == 'turn_left'):
			leftSpeed = 0
			rightSpeed = 100
		else:
			leftSpeed = 100
			rightSpeed = 100
	if starterThread:
		starterThread.join()
	if main_control.runDecisions:
		moves_control.keepRunning = True
		print('B4 new Starter thread!')
		starterThread = runThread("starterThread")
		starterThread.start()
		old_decision = dm
	else:
		main_control.runMoves = False

class runThread(threading.Thread):
	global leftSpeed
	global rightSpeed
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = name
	def run(self):
		if self.name == "starterThread":
			starter.run_start(leftSpeed, rightSpeed)
			
